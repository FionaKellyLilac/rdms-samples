# Sample python script for the CurveValuesBatch endpoint

import requests
import configparser
import sys

# config
# specify the full pathname of your config file here
configFile = 'C://RDMS//Python//config.ini'

try:
    config = configparser.ConfigParser()
    config.read(configFile)
    api = config['RDMS']['Api']
    headers = {'Authorization' : config['RDMS']['Key'] }
except:
    print('Request failed: Problems with the information in config file or the config file does not exist.')
    sys.exit(0)


# params
# specify in a json format as below 
  
jsonParam = {
            "totalMaxValues": 350,                      # optional parameter 
            "totalMaxForecasts": 15,                    # optional parameter 
            "scenarioID": 0,                            # optional parameter, sets the default value for all curve requests
            "minForecastDate": "2000-01-01",            # optional parameter, sets the default value for all curve requests
            "maxForecastDate": "2000-01-01",            # optional parameter, sets the default value for all curve requests
            "minValueDate": "2020-03-29",               # optional parameter, sets the default value for all curve requests
            "maxValueDate": "2020-04-28",               # optional parameter, sets the default value for all curve requests
            "resultTimezone": "UTC",                    # optional parameter, sets the default value for all curve requests
            "maxValues": 50,                            # optional parameter, sets the default value for all curve requests
            "maxForecasts": 6,                          # optional parameter, sets the default value for all curve requests
            "sortForecastsDateDescending": True,        # optional parameter, sets the default value for all curve requests
            "sortValuesDateDescending": True,           # optional parameter, sets the default value for all curve requests

            "curveRequests": [                          # mandatory parameter
            {
                "curveID": "111975",                    # mandatory parameter, must have at least one CurveID
            },
            {
                "curveID": "111976",
                "minValueDate": "2021-01-01",           # optional parameter, this overrides the default value above
                "maxValueDate": "2021-01-08",
            },
            {
                "curveID": "111977",
                "maxValues": 15,
                "minValueDate": "2022-01-01",
                "maxValueDate": "2022-01-08"
            },
        ]
    }



#make the request
fullURL = api + '/CurveValuesBatch/'
result = requests.post(fullURL,json=jsonParam, headers=headers, verify=True)

#display the results
if result.status_code == 200:
    jsonResult = result.json()

    for curveForecast in jsonResult['curveForecasts']:
        print ('CurveID : ' + curveForecast['request']['curveID'] )
        print ('Status is ' + curveForecast['status'])
        print ('Forecasts are ...')
        for forecast in curveForecast['forecasts']:
            for forecastValue in forecast['values']:
                print(forecastValue)
else:
	print('Request failed: ' + str(result.status_code))
