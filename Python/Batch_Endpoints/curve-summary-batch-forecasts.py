# Sample python script for the CurveSummaryBatch/Forecasts endpoint

import requests
import configparser
import sys

# config
# specify the full pathname of your config file here
configFile = 'C://RDMS//Python//config.ini'

try:
    config = configparser.ConfigParser()
    config.read(configFile)
    api = config['RDMS']['Api']
    headers = {'Authorization' : config['RDMS']['Key'] }
except:
    print('Request failed: Problems with the information in config file or the config file does not exist.')
    sys.exit(0)


# params
# specify in a json format as below
jsonParam = {
            "minForecastDate": "2000-01-01",                    # optional parameter, sets the default value for all curve requests
            "maxForecastDate": "2000-01-01",                    # optional parameter, sets the default value for all curve requests
            "valueDate": "2020-03-29",                          # optional parameter, sets the default value for all curve requests
            
            "curveRequests": [                                  # mandatory parameter
            {
                "curveID": "101814454",                         # mandatory parameter, must have at least one CurveID
            },
            {
                "curveID": "101769592",
                "minForecastDate": "2022-02-14",                # optional parameter, this overrides the default value above
                "maxForecastDate": "2022-02-16",
            },
            {
                "curveID": "101752647",
                "valueDate": "2022-01-10",
                "minForecastDate": "2022-01-01",
                "maxForecastDate": "2022-01-08"
            },
        ]
    }



# make the request
fullURL = api + '/CurveSummaryBatch/Forecasts/'
result = requests.post(fullURL,json=jsonParam, headers=headers, verify=True)

# display the results
if result.status_code == 200:
    jsonResult = result.json()

    for summary in jsonResult['curveForecastSummaries']:
        print ('CurveID : ' + summary['request']['curveID'] )
        print ('Status : ' + str(summary['status']))
        if (summary['forecastCount'] >0):
            print ('Forecast count : ' + str(summary['forecastCount']))
            print ('Min forecast date : ' + str(summary['minForecastDate']))
            print ('Max forecast date : ' + str(summary['maxForecastDate']))
else:
	print('Request failed: ' + str(result.status_code))
