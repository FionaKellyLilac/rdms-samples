# Sample python script for the CurveSummaryBatch/Values endpoint

import requests
import configparser
import sys

# config
# specify the full pathname of your config file here
configFile = 'C://RDMS//Python//config.ini'

try:
    config = configparser.ConfigParser()
    config.read(configFile)
    api = config['RDMS']['Api']
    headers = {'Authorization' : config['RDMS']['Key'] }
except:
    print('Request failed: Problems with the information in config file or the config file does not exist.')
    sys.exit(0)


# params
# specify in a json format as below

jsonParam = {
            
            "minValueDate": "2000-01-01",       # optional parameter, sets the default value for all curve requests
            "maxValueDate": "2000-01-01",       # optional parameter, sets the default value for all curve requestss

            "curveRequests": [                  # mandatory parameter
            {
                "curveID": "101814454",         # mandatory parameter, must have at least one CurveID
            },
            {
                "curveID": "101769592",
                "minValueDate": "2022-02-14",   # optional parameter, this overrides the default value above
                "maxValueDate": "2022-02-16",
            },
            {
                "curveID": "101752647",
                "minValueDate": "2022-01-01",
                "maxValueDate": "2022-01-08"
            },
        ]
    }



# make the request
fullURL = api + '/CurveSummaryBatch/Values/'
result = requests.post(fullURL,json=jsonParam, headers=headers, verify=True)

# display the results
# if no forecasts match the request the last updated time, min value date and max value date are set to '0001-01-01T00:00:00+00:00'
if result.status_code == 200:
    jsonResult = result.json()

    for summary in jsonResult['curveValueSummaries']:
        print ('CurveID : ' + summary['request']['curveID'] )
        print ('Status : ' + str(summary['status']))
        print ('Last update time : ' + str(summary['lastUpdateTime']))
        print ('Min value date : ' + str(summary['minValueDate']))
        print ('Max value date : ' + str(summary['maxValueDate']))
else:
	print('Request failed: ' + str(result.status_code))
