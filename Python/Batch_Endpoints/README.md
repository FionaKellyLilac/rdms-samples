
# Batch Endpoints #

* Batch endpoints enable you to fetch data for multiple curves in one request.

* These endpoints use the http POST method and the details are provided in a json formatted parameter.

* The key:value pairs provided set the defaults for all parameters.

* These defaults will be used for each curve in the curvesRequest array part of the json, unless an override is specified in the curvesRequest section.

* Each section within curveRequests is curve-specific and only needs to contain values in parameters that are NOT included in the default parameters (e.g. curveID).The default can be overridden on a per-curve basis within the curveRequest.

* The sample code is commented with which parameters are mandatory, which are optional and which are overriding the default value.

* Prior to running these samples edit the config file to contain your server name and your apikey, and edit the line of code starting 'configfile = ' to be the fullpathname of your config file.

* Please refer to your REST API guide for further details on the parameters available.



## Curve Values Batch ##

Returns raw values for the requested curves, optionally restricted by any of the other parameters.


## Curve Values Batch Latest Forecast ##

Returns latest forecasts for the requested curves, optionally restricted by any of the other parameters.


## Curve Values Batch Aggregated ##

Returns aggregated values for the requested curves.


## Curve Summary Batch Forecasts ##

For a set of curves, find the last update time and the minimum and maximum value dates available.


## Curve Summary Batch Values ##

For a set of curves, find the last update time and the minimum and maximum value dates available.


