# Sample python script for the CurveValuesBatch/Aggregated endpoint

import requests
import configparser
import sys

# config
# specify the full pathname of your config file here
configFile = 'C://RDMS//Python//config.ini'

try:
    config = configparser.ConfigParser()
    config.read(configFile)
    api = config['RDMS']['Api']
    headers = {'Authorization' : config['RDMS']['Key'] }
except:
    print('Request failed: Problems with the information in config file or the config file does not exist.')
    sys.exit(0)


# params
# specify in a json format as below

jsonParam = {
            "resultPeriodType": "Months",                       # optional parameter, but unlike the other optional parameters if not specified here it must be specified in each of the individual curve sections below
                                                                # if specified here it sets the default for all curve requests
            "resultPeriodLength": 1,                            # optional parameter, but unlike the other optional parameters if not specified here it must be specified in each of the individual curve sections below
                                                                # if specified here it sets the default for all curve requests
            "totalMaxValues": 350,                              # optional parameter
            "scenarioID": 0,                                    # optional parameter, sets the default value for all curve requests
            "maxForecastDate": "2022-02-16T14:05:44.241Z",      # optional parameter, sets the default value for all curve requests
            "resultTimezone": "UTC",                            # optional parameter, sets the default value for all curve requests
            "minValueDate": "2022-02-16T14:05:44.241Z",         # optional parameter, sets the default value for all curve requests
            "maxValueDate": "2022-02-16T14:05:44.241Z",         # optional parameter, sets the default value for all curve requests
            "calculateProportional": True,                      # optional parameter, sets the default value for all curve requests
            "excludeNulls": True,                               # optional parameter, sets the default value for all curve requests
            "sourceIsQuantity": True,                           # optional parameter, sets the default value for all curve requests
            "sourceTimezone": "UTC",                            # optional parameter, sets the default value for all curve requests
            "sourcePeriodType": "Months",                       # optional parameter, sets the default value for all curve requests
            "sourcePeriodLength": 1,                            # optional parameter, sets the default value for all curve requests
            "flatten": True,                                    # optional parameter, sets the default value for all curve requests
            "maxValues": 10,                                    # optional parameter, sets the default value for all curve requests
            "sortValuesDateDescending": True,                   # optional parameter, sets the default value for all curve requests

            "curveRequests": [                                  # mandatory parameter
                                                                # if resultPeriodType and/or resultPeriodLength are not specified above they must be specified individually for each curveID below
            {
                "curveID": "102238954",                         # mandatory parameter, must have at least one CurveID                                                                
            },
            {
                "curveID": "111976",
                "minValueDate": "2021-01-01",                   # optional parameter, this overrides the default value above
                "maxValueDate": "2021-01-08",
            },
            {
                "curveID": "111977",
                "maxValues": 15,
                "minValueDate": "2022-01-01",
                "maxValueDate": "2022-01-08"
            },
        ]
    }



#make the request
fullURL = api + '/CurveValuesBatch/Aggregated/'
result = requests.post(fullURL,json=jsonParam, headers=headers, verify=True)

#display the results
if result.status_code == 200:
    jsonResult = result.json()

    for aggregation in jsonResult['curveAggregations']:
        print ('CurveID : ' + aggregation['request']['curveID'] )
        print ('Last Updated Time : '+ str(aggregation['lastUpdateTime']))
        print ('Status : ' + str(aggregation['status']))
        for value in aggregation['values']:
            print(value)
else:
	print('Request failed: ' + str(result.status_code))
