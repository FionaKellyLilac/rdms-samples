import configparser
import requests
import pandas as pd


#config
config = configparser.ConfigParser()
try:
    # Adjust the line below if the config file is in another location
    config.read('C:\\RDMSPython\\config.ini')
    api = config['RDMS']['Api']
    headers = { 'Authorization' : config['RDMS']['Key'] }
except KeyError:
    print('Request failed: Problems with the information in config file or the config file location. More information can be found in the read me.')
    exit('Script will now stop.')

#Insert the CSV File name and Location Path Below
filename = ''
fileLocation = 'C:\\' 
csvFile = fileLocation + filename + ".csv"

try:
    propCurveData = pd.read_csv(csvFile)
except FileNotFoundError:
    print("Request failed: The CSV File '",csvFile,"' could not be found.")
    exit('Script will now stop.')

newCurve = {}
curveIDList = []

# remove leading and trailing spaces from columns
propCurveData = propCurveData.rename(columns=lambda x: x.strip()) 

acceptedMetadataHeaders = [ 'curveName','commodity', 'unitOfMeasure', 'valueFrequency', 'geography', 'geographyTo', 'variable', 'status', 'timeZone', 'source', 'priceArea', 'issueFrequency', 'facility', 'fuelTechnology', 'shortName', 'scenarioName', 'methodology']

for n in range(len(propCurveData)):

    for i in range(len(acceptedMetadataHeaders)):

        if acceptedMetadataHeaders[i] in propCurveData.columns:
            
            key = acceptedMetadataHeaders[i]
            newCurve[str(key)] = str(propCurveData[str(key)][n])

        else:
            
            continue

    key = 'securityGroup'
    newCurve[str(key)] = 'PUBLIC'
    
    result = requests.post(api + '/RegisterPropCurve', json=newCurve, headers=headers, verify=True)

    if result.status_code == 200:
        body = result.json()
        curveIDList.append(body['curveID'])
        print('Created ' + str(body['curveID']))

    else:
        print('HTTP ' + str(result.status_code) + ' failed to save')
        body = result.json()
        print(body)
        exit()

propCurveData['curveID'] = curveIDList

propCurveData.to_csv(fileLocation + filename + '_with_curveIDS.csv', index = False, header= True)