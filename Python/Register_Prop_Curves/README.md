# Register Prop Curves from CSV #

Before using this python script the config file needs to be completed and a few lines updated to correctly specify the location of the config file and csv file.


### Config File ###

#### STEP 1: ####

To use these examples, please copy into the directory C:\RDMSPython.  

If you would prefer to use another location change the code in line 9 to be correct for you.

config.read('<Your full pathname>\\config.ini')

#### STEP 2: ####

Edit the file config.ini for the RDMS server and API key you will be using, using the format below.
These details should have been supplied to you as part of your subscription to the service.

\[RDMS\]
Api = https://<RDMS_SERVER>/api/v1

Key = <APIKEY>

For example,

\[RDMS\]
Api = https://demo.example.com/api/v1

Key = apikey-v1 1-7Ig0JR0tv9Bv5gMg3AgKzLBONKSdj1gDORrOIQ7


### CSV File ###

A csv template is included in this folder, with the following headers

curveName,commodity,unitOfMeasure,valueFrequency,geography,geographyTo,variable,status,timeZone,source,priceArea,issueFrequency,facility,fuelTechnology,shortName,scenarioName,methodology

Edit the code in line 18 to be your filename without the .csv part.

Edit the code in line 19 to be the full pathname.




