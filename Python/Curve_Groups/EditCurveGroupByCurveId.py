# Sample python script for the CurveGroup endpoint

import requests
import configparser
import sys

# config
# specify the full pathname of your config file here
configFile = 'C://ConfigFiles//my_config.ini'

try:
    config = configparser.ConfigParser()
    config.read(configFile)
    api = config['RDMS']['Api']
    headers = {'Authorization' : config['RDMS']['Key'] }
except:
    print('Request failed: Problems with the information in config file or the config file does not exist.')
    sys.exit(0)


# params

# change to be the curveGroupId you are editing
curveGroupId = 91                                               # the curveGroupId you are editing
                                                               
# specify in a json format as below

jsonParam = {
            "userGroupName": "my_user_group",                   # an existing user group which you have permissions for, if not supplied this will default to your userGroupName
            "groupName": "group_name",                          # either the existing curve group name or a new unique curve group name for this curve group
            "useFilter": False,                                 # set to False, this must be the same as the existing curveGroup
            "permissionsForCurves": [                             
                                {"curveID": 80110111,     
                                "readMeta": False,
                                "writeMeta": False,
                                "readData" : False,
                                "writeData": False                                                                          
                                },
                                {"curveID": 80111000,             # An existing curveID              
                                "readMeta": True,                
                                "writeMeta": True,
                                "readData" : True,
                                "writeData":True  
                                },
        ]
    }



#make the request
fullURL = api +'v1/CurveGroup/'+ str(curveGroupId)
result = requests.post(fullURL,json=jsonParam, headers=headers, verify=True)

#display the results
if result.status_code == 200:
    print('Success')
else:
	print('Request failed : ' + str(result.status_code) + ' : ' + result.reason + ' : ' + result.text)
