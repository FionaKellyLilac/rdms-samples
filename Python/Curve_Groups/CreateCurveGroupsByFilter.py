# Sample python script for the CurveGroup endpoint

import requests
import configparser
import sys

# config
# specify the full pathname of your config file here
configFile = 'C://ConfigFiles//my_config.ini'

try:
    config = configparser.ConfigParser()
    config.read(configFile)
    api = config['RDMS']['Api']
    headers = {'Authorization' : config['RDMS']['Key'] }
except:
    print('Request failed: Problems with the information in config file or the config file does not exist.')
    sys.exit(0)


# params
# specify in a json format as below

jsonParam = {
            "userGroupName": "my_user_group",                       # an existing user group which you have permissions for, if not supplied this will default to your userGroupName
            "groupName": "new_group_name",                          # a unique curve group name for this curve group
            "useFilter": True,                                      # set to True
            "FilterSearch":"+Commodity=Gas +Geography.from=Spain",
            "FilterReadMeta": True,                                 # readMeta permissions for all the curves in this group
            "FilterWriteMeta": False,                               # writeMeta permissions for all the curves in this group
            "FilterReadData" : True,                                # readData permissions for all the curves in this group
            "FilterWriteData": False                                # writeData permissions for all the curves in this group                                                                  
                               
    }



#make the request
fullURL = api +'v1/CurveGroup'
result = requests.post(fullURL,json=jsonParam, headers=headers, verify=True)

#display the results
if result.status_code == 200:
    print('Success')
else:
	print('Request failed : ' + str(result.status_code) + ' : ' + result.reason + ' : ' + result.text)
